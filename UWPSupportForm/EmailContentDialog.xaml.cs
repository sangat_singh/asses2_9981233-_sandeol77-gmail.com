﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UWPSupportForm
{
    public sealed partial class EmailContentDialog : ContentDialog
    {
        public enum DialogResult
        {
            Cancel,
            Send,
            Reset
        }

        public DialogResult Result { get; set; }

        public EmailContentDialog(Email email)
        {
            this.InitializeComponent();

            ToTextBlock.Text = email.To;
            FromTextBlock.Text = email.From;
            SubjectTextBlock.Text = email.Subject;
            MessageTextBlock.Text = email.Message;

            SendButton.Content = "SEND to " + email.To;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Result = DialogResult.Cancel;
            this.Hide();
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            Result = DialogResult.Send;
            this.Hide();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Result = DialogResult.Reset;
            this.Hide();
        }
    }
}
