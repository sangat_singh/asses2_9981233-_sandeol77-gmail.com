﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static UWPSupportForm.EmailContentDialog;

namespace UWPSupportForm
{
    public sealed partial class MainPage : Page
    {
        // The list of subjects
        List<string> SubjectList;

        // The email object
        Email email;

        public MainPage()
        {
            this.InitializeComponent();

            // We set the preffered min window size
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(300, 100));

            // We prepare the subjects list
            SubjectList = new List<string>();
            SubjectList.Add("Subject with priority 1");
            SubjectList.Add("Subject with priority 2");
            SubjectList.Add("Subject with priority 3");
            SubjectList.Add("Subject with priority 4");
            SubjectList.Add("Subject with priority 5");
            SubjectComboBox.ItemsSource = SubjectList;
            SubjectComboBox.SelectedIndex = 0;

            LoadData();
        }

        // When the user clicks on the Submit button, show the alert box
        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            email = new Email(ToTextBox.Text, FromTextBox.Text, SubjectComboBox.SelectedItem.ToString(), MessageTextBox.Text);
            EmailContentDialog alertBox = new EmailContentDialog(email);
            await alertBox.ShowAsync();

            // Use the returned custom result
            if (alertBox.Result == DialogResult.Cancel)
            {
                // Do nothing here
            }
            else if (alertBox.Result == DialogResult.Reset)
            {
                ResetGUI();
            }
            else if (alertBox.Result == DialogResult.Send)
            {
                ResetGUI();
            }
        }

        // Fills the GUI with data
        private void LoadData()
        {
            ToTextBox.Text = "HR Department";
            FromTextBox.Text = "johndoe@jgmail.com";
            SubjectComboBox.SelectedIndex = 0;
            MessageTextBox.Text = "This is the message body.";
        }

        // Resets the GUI
        private void ResetGUI()
        {
            ToTextBox.Text = "";
            FromTextBox.Text = "";
            SubjectComboBox.SelectedIndex = 0;
            MessageTextBox.Text = "";
        }
    }
}
