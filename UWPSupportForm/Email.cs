﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWPSupportForm
{
    public class Email
    {
        #region Properties
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructors
        public Email(string to, string from, string subject, string message)
        {
            To = to;
            From = from;
            Subject = subject;
            Message = message;
        }
        #endregion
    }
}
